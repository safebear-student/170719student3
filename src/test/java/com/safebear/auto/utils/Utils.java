package com.safebear.auto.utils;


import org.openqa.selenium.WebDriver;
import org.openqa.selenium.chrome.ChromeDriver;
import org.openqa.selenium.chrome.ChromeOptions;

public class Utils {

    //the variable for the Url - as it is final, it can't be altered.
    private static final String URL = System.getProperty("url", "http://localhost:8080");

    //the variable for the Browser - as it is final, it can't be altered.
    private static final String BROWSER = System.getProperty("browser", "chrome");

    //method for returning the URL string
    public static String getUrl() {
        return URL;
    }

    //method for returning the BROWSER string - and assigning options to the driver
    public static WebDriver getDriver() {

        System.setProperty("webdriver.chrome.driver", "src/test/resources/drivers/chromedriver.exe");

        ChromeOptions options = new ChromeOptions();
        options.addArguments("window-size=1366,768");

        switch (BROWSER) {
            case "chrome":
                return new ChromeDriver(options);

            default:
                return new ChromeDriver(options);
        }

    }


}
